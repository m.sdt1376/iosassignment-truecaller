# Truecaller Software Engineer - iOS Technical Assignment

### General info

This project is the CodingTask of Trucellar company for the iOS Developer position.

### Technologies

1. Xcode version: 12.5
1. Swift version: 5
1. Clean Architecture(VIP)

### Libraries

1. Moya - an elegant interface for HTTP network requests in Swift 
1. PromisKit - is a library for simplifying asynchronous programming

### How to run the project?

1. Open a shell window and navigate to the project folder
1. Run pod install
1. Open ios-app-particle-setup.xcworkspace and run the project on a selected device or simulator

### Project Explanation

At some point, we need to perform UI updates in response to heavy work we’ve done on a background queue, and in this project, It is implemented with GCD and using .userInitiated quality-of-service.

```
DispatchQueue.global(qos: .userInitiated).async {
    var result = ""
    let n = 10
    for i in 1...(input.count/n) {
        result += self.getNthChar(input: input, n: i*n)
    }
    let viewModel = Home.Data.ViewModel(tag: tag, text: result)
    self.guaranteeMainThread {
        self.viewController?.displayData(viewModel: viewModel)
        self.hideLoading(response: .init(tag: tag))
    }
}
```

For efficiency in this project, I used Dictionary collection to count every word in plain text.

```
func calculateOccurrence(input: [String]) -> [String: Int] {
    var dictInfo = [String: Int]()
    for string in input {
        let current = string.lowercased()
        if let value = dictInfo[current] {
            dictInfo[current] = value + 1
        } else {
            dictInfo[current] = 1
        }
    }
    return dictInfo
}
```

Good luck!
