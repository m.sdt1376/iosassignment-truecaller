//
//  HomeInteractor.swift
//  iOSAssignment
//
//  Created by mohammadSaadat on 6/5/1400 AP.
//  Copyright (c) 1400 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol HomeBusinessLogic {
    func fetchData()
}

protocol HomeDataStore {}

class HomeInteractor: HomeDataStore {
    // MARK: - Object lifecycle
    init() {
        HomeLogger.logInit(owner: String(describing: HomeInteractor.self))
    }
    
    // MARK: - Deinit
    deinit {
        HomeLogger.logDeinit(owner: String(describing: HomeInteractor.self))
    }
    
    // MARK: - Properties
    
    // MARK: Public
    var presenter: HomePresentationLogic?
    var worker: HomeWorkerLogic?
}

// MARK: - Methods

// MARK: Private
private extension HomeInteractor {
    func presentError(_ error: Error) {
        self.presenter?.presentError(response: .init(error: error))
    }
    
    func dataFetched(data: String?, textViewTag: Int) {
        self.presenter?.presentData(response: .init(tag: textViewTag, text: data))
    }
    
    func TenthCharRequest() {
        worker?.getTenthChar()
            .done { self.dataFetched(data: $0, textViewTag: 0) }
            .catch(presentError)
    }
    
    func EveryTenthCharRequest() {
        worker?.getEveryTenthChar()
            .done { self.dataFetched(data: $0, textViewTag: 1) }
            .catch(presentError)
    }
    
    func WordCounterRequest() {
        worker?.getWorlCounter()
            .done { self.dataFetched(data: $0, textViewTag: 2) }
            .catch(presentError)
    }
}

// MARK: Public
extension HomeInteractor {}

// MARK: - Business Logics
extension HomeInteractor: HomeBusinessLogic {
    func fetchData() {
        presenter?.showLoading()
        TenthCharRequest()
        EveryTenthCharRequest()
        WordCounterRequest()
    }
}
