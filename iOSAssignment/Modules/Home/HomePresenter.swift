//
//  HomePresenter.swift
//  iOSAssignment
//
//  Created by mohammadSaadat on 6/5/1400 AP.
//  Copyright (c) 1400 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol HomePresentationLogic {
    func showLoading()
    func hideLoading(response: Home.Loading.Response)
    func presentData(response: Home.Data.Response)
    func presentError(response: Home.ErrorModel.Response)
}

class HomePresenter {
    // MARK: - Object lifecycle
    init(queue: DispatchQueue = DispatchQueue(label: "ConcurrentQueue", qos: .userInitiated, attributes: [.concurrent])) {
        HomeLogger.logInit(owner: String(describing: HomePresenter.self))
        self.queue = queue
    }
    
    // MARK: - Deinit
    deinit {
        HomeLogger.logDeinit(owner: String(describing: HomePresenter.self))
    }
    
    // MARK: - Properties
    
    // MARK: Public
    weak var viewController: HomeDisplayLogic?
    let queue: DispatchQueue
    
    private var textViewLoadingNumber: Int = 0
}

// MARK: - Methods

// MARK: Private
private extension HomePresenter {
    func guaranteeMainThread(_ work: @escaping (() -> Void)) {
        if Thread.isMainThread {
            work()
        } else {
            DispatchQueue.main.async(execute: work)
        }
    }
    
    func getNthChar(input: String, n: Int) -> String {
        let index = input.index(input.startIndex, offsetBy: n)
        if n < input.count  {
            return "\(n)th character : \(input[index]) \n"
        }
        return ""
    }
    
    func getEveryTenthChar(input: String, tag: Int, queue: DispatchQueue) {
        self.queue.async {
            var result = ""
            let n = 10
            for i in 1...(input.count/n) {
                result += self.getNthChar(input: input, n: i*n)
            }
            let viewModel = Home.Data.ViewModel(tag: tag, text: result)
            self.guaranteeMainThread {
                self.viewController?.displayData(viewModel: viewModel)
                self.hideLoading(response: .init(tag: tag))
            }
        }
    }
    
    func getWordCounter(input: String, tag: Int, queue: DispatchQueue) {
        self.queue.async {
            let array = input.components(separatedBy: CharacterSet(charactersIn: " \t\n"))
            let dictInfo = self.calculateOccurrence(input: array)
            var result = ""
            for (key, value) in dictInfo {
                result += "\(key) : appeared \(value) times\n"
            }
            let viewModel = Home.Data.ViewModel(tag: tag, text: result)
            self.guaranteeMainThread {
                self.viewController?.displayData(viewModel: viewModel)
                self.hideLoading(response: .init(tag: tag))
            }
        }
    }
}

// MARK: Public
extension HomePresenter {
    func calculateOccurrence(input: [String]) -> [String: Int] {
        var dictInfo = [String: Int]()
        for string in input {
            let current = string.lowercased()
            if let value = dictInfo[current] {
                dictInfo[current] = value + 1
            } else {
                dictInfo[current] = 1
            }
        }
        return dictInfo
    }
}

// MARK: - Presentation Logic
extension HomePresenter: HomePresentationLogic {
    func showLoading() {
        textViewLoadingNumber = 3
        guaranteeMainThread {
            self.viewController?.showLoading()
        }
    }
    
    func hideLoading(response: Home.Loading.Response) {
        textViewLoadingNumber -= 1
        let hideLoadingOnButton = textViewLoadingNumber == 0
        guaranteeMainThread {
            self.viewController?.hideLoading(viewModel: .init(tag: response.tag, hideLoadingOnButton: hideLoadingOnButton))
        }
    }
    
    func presentData(response: Home.Data.Response) {
        guard let text = response.text else { return }
        switch response.tag {
        case 0:
            let viewModel: Home.Data.ViewModel
            let string = getNthChar(input: text, n: 10)
            viewModel = .init(tag: response.tag, text: string)
            guaranteeMainThread {
                self.viewController?.displayData(viewModel: viewModel)
                self.hideLoading(response: .init(tag: response.tag))
            }
        case 1:
            getEveryTenthChar(input: text, tag: response.tag, queue: self.queue)
        case 2:
            getWordCounter(input: text, tag: response.tag, queue: self.queue)
        default:
            return
        }
    }
    
    func presentError(response: Home.ErrorModel.Response) {
        textViewLoadingNumber = 0
        guaranteeMainThread {
            self.viewController?.hideAllLoading()
            self.viewController?.displayError(viewModel: .init(error: response.error))
        }
    }
}
