//
//  HomeIntercatorTest.swift
//  iOSAssignmentTests
//
//  Created by mohammadSaadat on 6/6/1400 AP.
//

import XCTest
import PromiseKit
@testable import iOSAssignment

class HomeIntercatorTest: XCTestCase {
    
    var sut: HomeInteractor!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        setupHomeInteractor()
    }
    
    override func tearDownWithError() throws {
        sut = nil
        super.tearDown()
    }
    
    func setupHomeInteractor() {
        sut = HomeInteractor()
    }
    
    // MARK: - presenter
    class HomePresentationLogicTest: HomePresentationLogic {
        
        // MARK: Method call expectations
        var showLoadingCalled = false
        var hideLoadingeCalled = false
        var presentDataCalled = false
        var presentErrorCalled = false
        
        // MARK: presenter Logics methods
        func showLoading() {
            showLoadingCalled = true
        }
        
        func hideLoading(response: Home.Loading.Response) {
            hideLoadingeCalled = true
        }
        
        func presentData(response: Home.Data.Response) {
            presentDataCalled = true
        }
        
        func presentError(response: Home.ErrorModel.Response) {
            presentErrorCalled = true
        }
    }
    
    // MARK: - worker
    class HomeWorkerLogicTest: HomeWorkerLogic {
        // MARK: Method call expectations
        var getTenthCharCalled = false
        var getEveryTenthCharCalled = false
        var getWorlCounterCalled = false
        
        func getTenthChar() -> Promise<String?> {
            Promise { seal in
                getTenthCharCalled = true
                seal.fulfill("getTenthChar")
            }
        }
        
        func getEveryTenthChar() -> Promise<String?>{
            Promise { seal in
                getEveryTenthCharCalled = true
                seal.fulfill("getEveryTenthChargetEveryTenthChargetEveryTenthChar")
            }
        }
        
        func getWorlCounter() -> Promise<String?>{
            Promise { seal in
                getWorlCounterCalled = true
                seal.fulfill("test jj hello kkkkkkk \t hi \n 56")
            }
        }
    }
    
    // MARK: - Tests
    func testFetchDataShouldAskHomeWorkerToGetTreeRequestSimultaneously() {
        // Given
        let homePresentationLogicTest = HomePresentationLogicTest()
        sut.presenter = homePresentationLogicTest
        let homeWorkerLogicTest = HomeWorkerLogicTest()
        sut.worker = homeWorkerLogicTest
        
        // When
        sut.fetchData()
        
        // Then
        DispatchQueue.main.async {
            XCTAssert(homeWorkerLogicTest.getTenthCharCalled, "fetchData() should ask worker to fetch getTenthChar")
            XCTAssert(homeWorkerLogicTest.getEveryTenthCharCalled, "fetchData() should ask worker to fetch getEveryTenthChar")
            XCTAssert(homeWorkerLogicTest.getWorlCounterCalled, "fetchData() should ask worker to fetch getWorlCounter")
            XCTAssert(homePresentationLogicTest.showLoadingCalled, "fetchData() should ask presenter to show loading")
            XCTAssert(homePresentationLogicTest.presentDataCalled, "fetchData() should ask presenter to present data called")
            XCTAssert(homePresentationLogicTest.presentDataCalled, "fetchData() should ask presenter to present data called")
        }
    }
}

