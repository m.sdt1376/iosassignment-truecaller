//
//  HomePresenterTest.swift
//  iOSAssignmentTests
//
//  Created by mohammadSaadat on 6/6/1400 AP.
//

import XCTest
@testable import iOSAssignment

class HomePresenterTest: XCTestCase {
    
    var sut: HomePresenter!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        setupHomeInteractor()
    }
    
    override func tearDownWithError() throws {
        sut = nil
        super.tearDown()
    }
    
    func setupHomeInteractor() {
        sut = HomePresenter()
    }
    
    // MARK: - DisplayLogic
    class HomeDisplayLogicTest: HomeDisplayLogic {
        
        var zeroTagText = ""
        var oneTagText = ""
        var twoTagText = ""
        
        // MARK: Method call expectations
        
        var displayErrorCalled = false
        var displayDataCalled = false
        var hideAllLoadingCalled = false
        
        // MARK: Display Logics methods
        func displayError(viewModel: Home.ErrorModel.ViewModel) {
            displayErrorCalled = true
        }
        
        func showLoading() {
            
        }
        
        func hideLoading(viewModel: Home.Loading.ViewModel) {
            
        }
        
        func displayData(viewModel: Home.Data.ViewModel) {
            displayDataCalled = true
            switch viewModel.tag {
            case 0:
                zeroTagText = viewModel.text
            case 1:
                oneTagText = viewModel.text
            case 2:
                twoTagText = viewModel.text
            default:
                break
            }
        }
        
        func hideAllLoading() {
            hideAllLoadingCalled = true
        }
    }
    
    // MARK: - Tests
    func testPresentErrorShouldAskViewControllerToDisplayError() {
        // Given
        let homeDisplayLogicTest = HomeDisplayLogicTest()
        sut.viewController = homeDisplayLogicTest
        
        // When
        sut.presentError(response: Home.ErrorModel.Response(error: NetworkErrors.connectionTimeout))
        
        // Then
        XCTAssert(homeDisplayLogicTest.displayErrorCalled, "Presenting error should ask view controller to display error")
    }
    
    func testPresentDataShouldAskViewControllerToDisplayDataForTagZero() {
        // Given
        let homeDisplayLogicTest = HomeDisplayLogicTest()
        sut.viewController = homeDisplayLogicTest
        
        // When
        sut.presentData(response: .init(tag: 0, text: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam"))
        
        // Then
        XCTAssert(homeDisplayLogicTest.displayDataCalled, "Presenting data should ask view controller to display data")
        XCTAssertEqual(homeDisplayLogicTest.zeroTagText, "10th character : m \n", "zeroTagText should be display the 10th character")
    }
    
    func testPresentDataShouldAskViewControllerToDisplayDataForTagOne() {
        // Given
        let homeDisplayLogicTest = HomeDisplayLogicTest()
        sut.viewController = homeDisplayLogicTest
        
        // When
        sut.presentData(response: .init(tag: 1, text: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam"))
        sut.queue.sync { }
        
        
        // Then
        let result = "10th character : m \n20th character : t \n30th character : l \n40th character : s \n50th character : c \n60th character : i \n70th character : p \n80th character : d \n90th character :   \n100th character : c \n110th character : t \n120th character : t \n130th character : a \n140th character : a \n150th character :   \n160th character : v \n"
        DispatchQueue.main.async {
            XCTAssert(homeDisplayLogicTest.displayDataCalled, "Presenting data should ask view controller to display data")
            XCTAssertEqual(homeDisplayLogicTest.twoTagText, result, "OneTagText should be display the result")
        }
    }
    
    func testcalculateOccurrence() {
        // Given
        let homeDisplayLogicTest = HomeDisplayLogicTest()
        sut.viewController = homeDisplayLogicTest
        
        // When
        let result = sut.calculateOccurrence(input: ["1", "1", "2", "test", "hi", "oh"])
        
        
        // Then
        XCTAssertEqual(result["1"], 2, "1 appeaerd 2 times in string")
    }
}
