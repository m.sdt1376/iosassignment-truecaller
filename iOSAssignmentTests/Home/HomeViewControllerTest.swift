//
//  HomeViewControllerTest.swift
//  iOSAssignmentTests
//
//  Created by mohammadSaadat on 6/6/1400 AP.
//

import XCTest
@testable import iOSAssignment

class HomeViewControllerTest: XCTestCase {
    
    var sut: HomeViewController!
    var window: UIWindow!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        window = UIWindow()
        setupHomeViewController()
    }
    
    override func tearDownWithError() throws {
        window = nil
        sut = nil
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupHomeViewController() {
        let homeDpendencyContainer = HomeDependencyContainer()
        sut = homeDpendencyContainer.makeHomeViewController()
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
    
    // MARK: - interactor
    class HomeInteractorTest: HomeBusinessLogic {
        
        // MARK: Method call expectations
        var fetchDataCalled = false
    
        // MARK: Business Logics methods
        
        func fetchData() {
            fetchDataCalled = true
        }
    }
    
    // MARK: - Tests
    func testShouldFetchDataWhenViewDidAppear() {
        // Given
        let homeInteractorTest = HomeInteractorTest()
        sut.interactor = homeInteractorTest
        loadView()
        
        // When
        sut.viewDidAppear(true)
        
        // Then
        XCTAssert(homeInteractorTest.fetchDataCalled, "Should called fetch data right after the view appears")
        
    }
}
